The project is a simple desktop application with a simple GUI.
First, launch the skript. This will open a window with two entry fields.
Enter your Character and Realm name and then press the button. This will verify your input and then load mount data.
Afterwards press the new button, this will go through all the uncollected mounts of your character and filter through them.
This step might take long, maybe even several minutes.
Afterwards you will be greeted with a treeview of the mounts you have yet to collect.
Double-clicking on a row will display mount details, together with the picture of said mount.