import io
import requests
import tkinter as tk
import PIL
from PIL import ImageTk, Image
from tkinter import *
from tkinter import ttk
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import BackendApplicationClient

def get_character_mounts(character_name, realm_name, access_token):
    url = f"https://eu.api.blizzard.com/profile/wow/character/{realm_name}/{character_name}/collections/mounts?namespace=profile-eu&locale=en_US&access_token={access_token}"
    response = requests.get(url)
    data = response.json()
    if "mounts" not in data:
        return "Not found"
    return data["mounts"]

def get_all_mounts(access_token):
    url = f"https://eu.api.blizzard.com/data/wow/mount/index?namespace=static-eu&locale=en_US&access_token={access_token}"
    response = requests.get(url)
    data = response.json()
    return data["mounts"]

def get_access_token():
    client_id = "9326f65252424c33b8673bf23e2eb667"
    client_secret = "q9sjrfx2TREsoOLPuYjtoG897Cctqnra"
    client = BackendApplicationClient(client_id=client_id)
    oauth = OAuth2Session(client=client)
    token_url = "https://eu.battle.net/oauth/token"
    token = oauth.fetch_token(token_url=token_url, client_id=client_id, client_secret=client_secret)
    return token["access_token"]

def get_uncollected_mount_ids(character_mounts, all_mounts):
    collected_mount_ids = [mount["mount"]["id"] for mount in character_mounts]
    all_mount_ids = [mount["id"] for mount in all_mounts]
    uncollected_mount_ids = list(set(all_mount_ids) - set(collected_mount_ids))
    return uncollected_mount_ids

def get_drop_mounts(uncollected_mount_ids,access_token):
    mounts = []
    for id in uncollected_mount_ids:
        url = f"https://eu.api.blizzard.com/data/wow/mount/{id}?namespace=static-10.1.0_49255-eu&locale=en_US&access_token={access_token}"
        response = requests.get(url)
        data = response.json()
        if "source" in data and data["source"]["type"] == "DROP" and "requirements" not in data and "should_exclude_if_uncollected" not in data:
            mount = [data["id"],data["name"],data["description"],data["creature_displays"][0]["id"]]
            mounts.append(mount)
    return mounts
class App:
    def __init__(self, root):
        self.access_token = get_access_token()
        self.character_mounts = ""
        self.all_mounts = ""
        self.fr = Frame(root)
        self.fr.place(relx = 0, rely=0, relwidth = 1, relheight=1)
        self.username_label = tk.Label(self.fr, text="Username:")
        self.username_label.place(relx = 0.2, rely = 0.1,relwidth=0.6,relheight=0.05)
        self.username_entry = tk.Entry(self.fr)
        self.username_entry.place(relx = 0.2, rely = 0.2,relwidth=0.6,relheight=0.05)
        self.realm_label = tk.Label(self.fr, text="Realm:")
        self.realm_label.place(relx = 0.2, rely = 0.3,relwidth=0.6,relheight=0.05)
        self.realm_entry = tk.Entry(self.fr)
        self.realm_entry.place(relx = 0.2, rely = 0.4,relwidth=0.6,relheight=0.05)
        self.btn1 = tk.Button(self.fr, text="Search",command=self.search)
        self.btn1.place(relx = 0.2, rely = 0.5,relwidth=0.6,relheight=0.05)
    def search(self):
        charname = self.username_entry.get()
        realmname = self.realm_entry.get()
        self.character_mounts = get_character_mounts(charname.lower(), realmname.lower(),self.access_token)
        if self.character_mounts == "Not found":
            self.status_label = tk.Label(self.fr, text="Character or realm does not exist.")
            self.status_label.place(relx = 0.2, rely = 0.6,relwidth=0.7,relheight=0.05)
        else:
            self.all_mounts = get_all_mounts(self.access_token)
            self.status_label = tk.Label(self.fr, text="Character data loaded, you can proceed (This will take a while).")
            self.status_label.place(relx = 0.2, rely = 0.6,relwidth=0.7,relheight=0.05)
            self.btn2 = tk.Button(self.fr, text="Show me mounts I do not have",command=self.view_mounts)
            self.btn2.place(relx = 0.2, rely = 0.8,relwidth=0.6,relheight=0.1)
    def view_mounts(self):
        self.uncollected_mounts = get_uncollected_mount_ids(self.character_mounts,self.all_mounts)
        self.drop_mounts = get_drop_mounts(self.uncollected_mounts,self.access_token)
        self.win = Toplevel(width=1000,height=500)
        self.win.grab_set()
        self.win.transient(self.fr)
        self.fr2 = Frame(self.win)
        self.fr2.place(relx = 0, rely=0, relwidth =1, relheight=1)
        self.tree = ttk.Treeview(self.fr2)
        self.tree["columns"] = ("name", "description","imgid")
        self.tree.column("#0", width=5)
        self.tree.column("name", width=10)
        self.tree.column("description", width=300)
        self.tree.heading("name", text="Name")
        self.tree.heading("description", text="Description")
        for mount in self.drop_mounts:
            self.tree.insert("", "end", text=mount[0], values=(mount[1],mount[2],mount[3]))
        self.tree.bind("<Double-1>", lambda event: self.show_mount_details())
        self.tree.place(relx = 0, rely=0, relwidth = 1, relheight=1)
    def show_mount_details(self):
        self.win2 = Toplevel(width=1000,height=500)
        self.win2.grab_set()
        self.win2.transient(self.fr2)
        self.fr3 = Frame(self.win2)
        self.fr3.place(relx = 0, rely=0, relwidth =1, relheight=1)
        selected_item = self.tree.focus()
        mount_data = self.tree.item(selected_item)["values"]
        self.name_label = tk.Label(self.fr3, text=mount_data[0])
        self.name_label.place(relx = 0.2, rely = 0.2,relwidth=0.7,relheight=0.05)
        self.desc_label = tk.Label(self.fr3, text=mount_data[1])
        self.desc_label.place(relx = 0.2, rely = 0.4,relwidth=0.7,relheight=0.05)
        url = f"https://render.worldofwarcraft.com/eu/npcs/zoom/creature-display-{mount_data[2]}.jpg"
        response = requests.get(url)
        img = PIL.Image.open(io.BytesIO(response.content))
        resized_img = img.resize((300, 300))
        self.photo = ImageTk.PhotoImage(resized_img)
        self.photo_label = tk.Label(self.fr3, image=self.photo)
        self.photo_label.place(relx = 0.2, rely = 0.5,relwidth=0.7,relheight=0.5)

if __name__ == "__main__":
    root = Tk()
    root.title("Mount Collector")
    root.geometry("500x500")
    app = App(root)
    root.mainloop()
   # access_token = get_access_token()
   # character_name = input()
   # realm_name = input()
   # character_mounts = get_character_mounts(character_name.lower(), realm_name.lower(),access_token)
   # character_mounts = get_character_mounts("stynkey", "ragnaros",access_token)
   # all_mounts = get_all_mounts(access_token)
   # uncollected_mount_ids = get_uncollected_mount_ids(character_mounts,all_mounts)
   # drop_mounts = get_drop_mounts(uncollected_mount_ids,access_token)
   # print(drop_mounts)