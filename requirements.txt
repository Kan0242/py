attrs==22.2.0
certifi==2023.5.7
cffi==1.15.1
charset-normalizer==3.1.0
click==8.1.3
colorama==0.4.6
exceptiongroup==1.1.1
idna==3.4
iniconfig==2.0.0
oauthlib==3.2.2
packaging==23.0
Pillow==9.5.0
pluggy==1.0.0
pycparser==2.21
pydot==1.4.2
pygame==2.3.0
pymunk==6.4.0
pyparsing==3.0.9
pytest==7.2.2
requests==2.30.0
requests-oauthlib==1.3.1
tomli==2.0.1
urllib3==2.0.2
